/*
	Preparations:
	npm init -y
	npm install express
	npm install mongoose
*/


// require the installed modules
const express = require("express");
const mongoose = require("mongoose");

// port
const port = 3001;

// server
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch230.hqlo3o4.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes Grouping - organize the access for each resources.
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes); //localhost:4000/tasks/


// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`))

// model >> controller >> route >> application









/*
	Preparations:
	npm init -y
	npm install express
	npm isntall mongoose
*/

// require the installed modules
// const express = require("express");
// const mongoose = require("mongoose");

// const taskRoutes = require("./routes/taskRoutes");

// // port
// const port = 3000

// // server
// const app = express();

// // MongoDB conenction
// mongoose.connect("mongodb+srv://admin:admin@batch230.hqlo3o4.mongodb.net/s36?retryWrites=true&w=majority",
// 	{
// 		useNewUrlParser: true,
// 		useUnifiedTopology: true
// 	}
// );

// let db = mongoose.connection; // built-in yung .connection

// db.on("error", console.error.bind(console, "connection error"));
// db.once("open", () => console.log("Were connected to the cloud database"));


// app.use("./tasks", taskRoutes); // localhost:4000/tasks

// /* app.listen(port number)*/
// app.listen(port, () => console.log(`Server is running at port ${port}`));